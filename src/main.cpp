#ifndef ESP32
#include <iostream>
#include <cmath>
#include "homework01.h"
using namespace std;
#endif

#ifdef ESP32
void setup()
{
}
void loop()
#else
int main(int argc, char** argv)
#endif
{
#ifndef ESP32
  cout << "Test - OK" << endl;
  cout << "Cos(1.2) = " << cos(1.2) << endl;

  return 0;
#endif
}
